<?php
    session_start();

    if(isset($_POST['assign'])) {
        // Setting up a Database Connection
        $databaseServer = "localhost";
        $databaseUsername = "root";
        $databasePassword = "";
        $databaseName = "schoolsystem";

        $database_connection = new mysqli($databaseServer, $databaseUsername, $databasePassword, $databaseName);
        
        // Outputting an error incase the connection was not successful.
        if ($database_connection -> connect_error) {
            die("Unsuccessful Connection / Error: " . $database_connection -> connect_error);
        } else {
            $studentID = $_POST['StudentID'];
            $moduleID = $_POST['ModuleID'];
            $gradeValue = $_POST['GradeValue'];

            $username = $_SESSION['username'];

            $query = $database_connection -> query("SELECT * FROM PERSON WHERE Username='$username'");
            $user = $query -> fetch_assoc();
            
            // Checking if any of the textboxes are empty.
            if (empty($studentID) || empty($moduleID) || empty($gradeValue)) {
                header("Location: ../panel.php?assign=empty");
                exit();
            }

            // Checking if grade is between 0 and 100
            if ($gradeValue < 0 || $gradeValue > 100) {
                header("Location: ../panel.php?assign=invalidgrade");
                exit();
            }

            // Checking if StudentID exists in the table
            $query_result = $database_connection -> query("SELECT * FROM PERSON WHERE ID='$studentID'");

            if ($query_result -> num_rows == 0) {
                header("Location: ../panel.php?assign=invalidstudent");
                exit();
            }

            // Checking if the ModuleID exists in the table
            $query_result = $database_connection -> query("SELECT * FROM MODULES WHERE ModuleID='$moduleID'");

            if ($query_result -> num_rows == 0) {
                header("Location: ../panel.php?assign=invalidmodule");
                exit();
            }

            // Checking if the StudentID entered is studying the specific module.
            $query_result = $database_connection -> query("SELECT * FROM MODULES_STUDENTS WHERE ModuleID='$moduleID' AND StudentID='$studentID'");

            if ($query_result -> num_rows == 0) {
                header("Location: ../panel.php?assign=studentmismatch");
                exit();
            }

            $teacherID = $user['ID'];

            // Checking if the Teacher is teaching that specific module.
            $query_result = $database_connection -> query("SELECT * FROM MODULES_TEACHERS WHERE ModuleID='$moduleID' AND TeacherID='$teacherID'");

            if ($query_result -> num_rows == 0) {
                header("Location: ../panel.php?assign=unauthorizedmodule");
                exit();
            }

            // Inserting the new GRADE INTO GRADES TABLE
            $query_result = $database_connection -> query("INSERT INTO GRADES (GradeValue) VALUES ($gradeValue)");
            $last_grade_id = $database_connection -> insert_id;

            // Linking the new GradeID with the Student
            $query_result2 = $database_connection -> query("INSERT INTO GRADES_STUDENTS (GradeID, StudentID) VALUES ($last_grade_id, $studentID)");
            
            // Linking the new GradeID with the ModuleID
            $query_result3 = $database_connection -> query("INSERT INTO GRADES_MODULES (GradeID, ModuleID) VALUES ($last_grade_id, $moduleID)");
            
            if($query_result && $query_result2 && $query_result3) {
                header("Location: ../panel.php?assign=success");
                exit();
            }
        }

        // Closing the database connection
        $database_connection -> close();
    }

    session_write_close();

?>