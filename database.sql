-- database.sql
-- Created by Alexis Chrysostomou (ac01271)

-- Creating the database.
CREATE DATABASE IF NOT EXISTS schoolsystem;

-- Creating The Tables

-- PERSON TABLE (SUPERTYPE)
-- Contains all the attributes of a person entity.
CREATE TABLE IF NOT EXISTS PERSON (
    ID INT NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(20) NOT NULL,
    Surname VARCHAR(20) NOT NULL,
    DoB DATE NOT NULL,
    Username VARCHAR(20),
    Password VARCHAR(20),

    PRIMARY KEY(ID)
);

-- STUDENTS TABLE (SUBTYPE)
-- Linked with the PERSON Table via the StudentID which acts as a foreign key.
CREATE TABLE IF NOT EXISTS STUDENTS (
    StudentID INT NOT NULL,
    ClassLevel VARCHAR(10) DEFAULT 'A',

	UNIQUE INDEX(StudentID),
    FOREIGN KEY(StudentID) REFERENCES PERSON(ID)
);

-- TEACHERS TABLE (SUBTYPE)
-- Linked with the PERSON TABLE via TeacherID which acts as a foreign key.
CREATE TABLE IF NOT EXISTS TEACHERS (
    TeacherID INT NOT NULL,
    DateJoined DATE NOT NULL,

	UNIQUE INDEX(TeacherID),
    FOREIGN KEY(TeacherID) REFERENCES PERSON(ID)
);

-- PERSON_TELEPHONE TABLE (Multi-valued Attribute)
-- May store multiple telephone numbers for a single person entity.
CREATE TABLE IF NOT EXISTS PERSON_TELEPHONE (
    PersonID INT NOT NULL,
    Telephone VARCHAR(12) NOT NULL,

    FOREIGN KEY(PersonID) REFERENCES PERSON(ID)
);

-- GRADES TABLE (Independent Entity)
-- Stores information about each grade.
CREATE TABLE IF NOT EXISTS GRADES (
    GradeID INT NOT NULL AUTO_INCREMENT,
    GradeValue INT DEFAULT 0,

    PRIMARY KEY(GradeID)
);

-- GRADES_STUDENT (Many-To-Many Relation)
-- Links each Grade with a Student Entity.
CREATE TABLE IF NOT EXISTS GRADES_STUDENTS (
    GradeID INT NOT NULL,
    StudentID INT NOT NULL,

    FOREIGN KEY(StudentID) REFERENCES PERSON(ID),
    FOREIGN KEY(GradeID) REFERENCES GRADES(GradeID)
);

-- MODULES TABLE (Independent Entity)
-- Holds information about each Module.
CREATE TABLE IF NOT EXISTS MODULES (
    ModuleID INT NOT NULL AUTO_INCREMENT,
    ModuleName VARCHAR(20) NOT NULL,
    ModuleCode VARCHAR(20) NOT NULL,

    PRIMARY KEY(ModuleID)
);

-- MODULES_STUDENTS TABLE (Many-To-Many Relation)
-- Links each Student With The Module They're Studying.
CREATE TABLE IF NOT EXISTS MODULES_STUDENTS (
    ModuleID INT NOT NULL,
    StudentID INT NOT NULL,

    FOREIGN KEY(ModuleID) REFERENCES MODULES(ModuleID),
    FOREIGN KEY(StudentID) REFERENCES PERSON(ID)
);

-- GRADES_MODULES TABLE
-- Links each grade with it's respective module.
CREATE TABLE IF NOT EXISTS GRADES_MODULES (
    GradeID INT NOT NULL,
    ModuleID INT NOT NULL,

    FOREIGN KEY(GradeID) REFERENCES GRADES(GradeID),
    FOREIGN KEY(ModuleID) REFERENCES MODULES(ModuleID)
);


-- MODULES_TEACHERS TABLE (Many-To-Many Relation)
-- Links each Teacher with the Module they're teaching.
CREATE TABLE IF NOT EXISTS MODULES_TEACHERS (
    ModuleID INT NOT NULL,
    TeacherID INT NOT NULL,

    FOREIGN KEY(ModuleID) REFERENCES MODULES(ModuleID),
    FOREIGN KEY(TeacherID) REFERENCES PERSON(ID)
);

-- SEMINARS TABLE (Independent Entity)
-- Holds information about each Seminar.
CREATE TABLE IF NOT EXISTS SEMINARS (
    SeminarID INT NOT NULL AUTO_INCREMENT,
    SeminarName VARCHAR(20) DEFAULT 'None',
    DateHeld DATE NOT NULL,

    PRIMARY KEY(SeminarID)
);

-- SEMINARS_TEACHERS TABLE (Many-To-Many Relation)
-- Links each Teacher with the Seminar They Have Attended.
CREATE TABLE IF NOT EXISTS SEMINARS_TEACHERS (
    SeminarID INT NOT NULL,
    TeacherID INT NOT NULL,

    FOREIGN KEY(SeminarID) REFERENCES SEMINARS(SeminarID),
    FOREIGN KEY(TeacherID) REFERENCES PERSON(ID)
);

-- Prepopulating the tables with random data manually.

-- Filling Person Table with 2 students and 2 teachers.
INSERT INTO PERSON (FirstName, Surname, DoB, Username, Password) 
    VALUES 
        ('Jake', 'Woodson', '2001-05-01', 'jk005', 'Jk123s'),
        ('Paul', 'Harrison', '2001-09-02', 'ph002', 'Ph233s'),
        ('Roger', 'Williams', '1970-05-07', 'rgw000', 'Rg123p'),
        ('George', 'Hillson', '1980-02-01', 'gh123', 'Gh123p');

-- Specifying student Ids
INSERT INTO STUDENTS (StudentID, ClassLevel)
    VALUES
        (1, 'A'),
        (2, 'B');

-- Specifying Teacher Ids
INSERT INTO TEACHERS (TeacherID, DateJoined)
    VALUES
        (3, '2017-04-10'),
        (4, '2018-02-01');

-- Filling all the phone numbers of each person 
-- (each person may have multiple phone numbers)
INSERT INTO PERSON_TELEPHONE (PersonID, Telephone)
    VALUES
        (1, '077070427'),
        (2, '075033223'),
        (3, '079223333'),
        (3, '079222343'),
        (4, '078503233');

-- Filling random modules
INSERT INTO MODULES (ModuleName, ModuleCode)
    VALUES
        ('Biology', 'CM100'),
        ('Mathematics', 'CM101'),
        ('Computing', 'CM102'),
        ('Philosophy', 'CM103');

-- Linking each student with a module.
INSERT INTO MODULES_STUDENTS (ModuleID, StudentID)
    VALUES
        (1, 1),
        (2, 1),
        (3, 1),
        (1, 2),
        (3, 2),
        (4, 2);

-- Linking each module with a teacher
INSERT INTO MODULES_TEACHERS (ModuleID, TeacherID)
    VALUES
        (1, 3),
        (2, 3),
        (3, 4),
        (4, 4);

-- Filling all the grades of Students.
INSERT INTO GRADES (GradeValue)
    VALUES
        (70),
        (80),
        (90),
        (100);

-- Linking each grade with each Student.
INSERT INTO GRADES_STUDENTS (GradeID, StudentID)
    VALUES
        (1, 1),
        (2, 2),
        (3, 1),
        (4, 2);

-- Linking each grade with a module.
INSERT INTO GRADES_MODULES (GradeID, ModuleID)
    VALUES
        (1, 1),
        (2, 2),
        (3, 4),
        (4, 3);

-- Filling random information about each seminar.
INSERT INTO SEMINARS (SeminarName, DateHeld)
    VALUES
        ('Teaching', '2018-09-05'),
        ('Motivation', '2018-06-02');

-- Linking each Seminar with a teacher.
INSERT INTO SEMINARS_TEACHERS (SeminarID, TeacherID)
    VALUES
        (1, 3),
        (2, 4);