<!-- 
    index.php

    @author: Alexis Chrysostomou
-->
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <title>School Management System</title>
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    </head>

    <body>
        <div class="accountloginForm">
            <p class="info">* Login Credentials Are Provided In The Coursework Report (Page: 5)</p>
            <h2 class="title">School Management System</h2>
            
            <form class="loginForm" method="POST" action="signin.php">
                <input type="textbox" name="username" placeholder="Username"/>
                <input type="password" name="password" placeholder="Password"/>
                <input type="submit" name="login" value="Login"/>
            </form>

            <?php
                session_start();

                // If logged in redirect to panel.php
                if(isset($_SESSION['username'])) {
                    header("Location: ../panel.php");
                    exit();
                }
                
                // Printing Error Messages

                if(!isset($_GET['login'])) {
                    exit();
                } else {
                    $result = $_GET['login'];

                    if ($result == 'invalidcredentials') {
                        echo "<p>Invalid Credentials</p>";
                    } elseif ($result == 'empty') {
                        echo "<p>Empty Credentials</p>";
                    } elseif ($result == 'invalidcharacters') {
                        echo "<p>Invalid Characters</p>";
                    }
                }

                session_write_close();
            ?>
            
        </div>
    </body>
</html>