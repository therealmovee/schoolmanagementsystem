<?php
    session_start();

    // Only accessed via POST METHOD
    if(isset($_POST['logout'])) {
        // Destroying the session.
        session_unset();
        session_destroy();

        header("Location: ../index.php");
        exit();
    }

    session_write_close();
?>