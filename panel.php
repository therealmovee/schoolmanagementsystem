<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <title>School Management System</title>
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    </head>

    <body>
        <?php
            session_start();

            // Returning back to front page if the user is not logged in
            if(!isset($_SESSION['username'])) {
                header("Location: ../index.php");
            }

            // Establishing a connection with the database
            $databaseServer = "localhost";
            $databaseUsername = "root";
            $databasePassword = "";
            $databaseName = "schoolsystem";

            $database_connection = new mysqli($databaseServer, $databaseUsername, $databasePassword, $databaseName);

            $username = $_SESSION['username'];

            // Storing all the information about the user.
            if($_SESSION['isTeacher']) {
                $query_result = $database_connection -> query("SELECT * FROM PERSON INNER JOIN TEACHERS ON PERSON.ID=TEACHERS.TeacherID WHERE USERNAME='$username'");
            } elseif(!$_SESSION['isTeacher']) {
                $query_result = $database_connection -> query("SELECT * FROM PERSON INNER JOIN STUDENTS ON PERSON.ID=STUDENTS.StudentID WHERE USERNAME='$username'");
            }

            $user = $query_result -> fetch_assoc();

            // Clearing the query result
            $query_result -> free_result();
        ?>

        <form class="signout" method="POST" action="logout.php">
            <input type="submit" name="logout" value="Logout" />
        </form>

        <!-- Outputting Personal Information from the Database -->
        <div class="personalInformation">
            <h2>Personal Information</h2>
            <p><b>User ID:</b> <?php echo $user['ID']; ?> </p>
            <p><b>First Name:</b> <?php echo $user['FirstName']; ?> </p>
            <p><b>Surname:</b> <?php echo $user['Surname']; ?> </p>
            <p><b>Date Of Birth:</b> <?php echo $user['DoB']; ?> </p>
            <p><b>Telephone Number(s): </b></p>
            <?php
                $telephone_query = $database_connection -> query("SELECT Telephone FROM PERSON_TELEPHONE WHERE PersonID IN (SELECT ID FROM PERSON WHERE Username='$username')");
                
                while($telephone = $telephone_query -> fetch_assoc()) {
                    echo '<p>' . $telephone['Telephone'] . '</p>';
                }

                // Clearing the results of all the queries
                $telephone_query -> free_result();

            ?>
            </p>
            <p><b>Account Type:</b> 

            <?php 
                // Checks if the user is a student or a teacher.
                if(!$_SESSION['isTeacher']) {
                    echo 'Student';
                } else {
                    echo 'Teacher';
                }
            ?>
            </p>
            <p>
               <?php 
                    if(!$_SESSION['isTeacher']) {
                        echo '<b>ClassLevel: </b> ' . $user['ClassLevel'];
                    } else {
                        echo '<b>Date Joined: </b>' . $user['DateJoined'];
                    }

               ?>
            </p>
        </div>

        <?php
            // Modules Information (For Students)
            if (!$_SESSION['isTeacher']) {
                echo "<div class='modules'>";
                echo "<h2>Modules</h2>";
                echo "</div>";

                // Getting all the modules of the student
                $user_id = $user['ID'];

                // Using a sub query to get the ModuleName
                $query_result = $database_connection -> query("SELECT * FROM MODULES WHERE ModuleID IN (SELECT ModuleID FROM MODULES_STUDENTS WHERE StudentID='$user_id')");
                
                while($module = $query_result -> fetch_assoc()) {
                    $moduleName = $module['ModuleName'];

                    echo '<p>' . $moduleName . ' (' . $module['ModuleCode'] . ')</p>';
                }

                // Clearing the query result
                $query_result -> free_result();

            } else {
                echo "<div class='modules'>";
                echo "<h2>Teaching Modules</h2>";
                echo "</div>";

                // Getting all the modules of the student
                $user_id = $user['ID'];

                $query_result = $database_connection -> query("SELECT * FROM MODULES_TEACHERS WHERE TeacherID='$user_id'");

                // Printing all the modules
                while ($query_module = $query_result -> fetch_assoc()) {
                    $moduleID = $query_module['ModuleID'];
                    
                    // Matching the ModuleID from MODULE_STUDENTS TABLE to MODULES table
                    $module_query = $database_connection -> query("SELECT * FROM MODULES WHERE ModuleID='$moduleID'"); 
                    $module = $module_query -> fetch_assoc();

                    $moduleName = $module['ModuleName'];

                    echo "<p>$moduleName (Module ID: $moduleID)</p>";
                    echo "<b>Students:</b> ";

                    // Looking for all students of each module.
                    $student_module_query = $database_connection -> query("SELECT * FROM MODULES_STUDENTS WHERE ModuleID ='$moduleID'");

                    while($query_student = $student_module_query -> fetch_assoc()) {
                        $student_id = $query_student['StudentID'];

                        // Matching the Student ID From MODULES_STUDENTS To PERSON TABLE
                        $student_query = $database_connection -> query("SELECT * FROM PERSON WHERE ID ='$student_id'");
                        $student = $student_query -> fetch_assoc();

                        // Printing all students along with their ID.
                        $studentdetails = $student['FirstName'] . ' ' . $student['Surname'] . ' (ID: ' . $student['ID'] . ') ';
                        echo "$studentdetails";
                    }
                }

                // Clearing the query result
                $query_result -> free_result();
                             
            }

            // Grades Information (For Students)
            if (!$_SESSION['isTeacher']) {
                echo "<div class='grades'>";
                echo "<h2>Grades</h2>";
                echo "</div>";

                $user_id = $user['ID'];

                $query_result = $database_connection -> query("SELECT * FROM GRADES_STUDENTS WHERE StudentID ='$user_id'");
               
                // Matching all the Grade IDs From GRADE_STUDENTS TO GRADES TABLE
                while ($grades = $query_result -> fetch_assoc()) {
                    $grade_id = $grades['GradeID'];

                    // Getting the module ID.
                    $grade_module_id_query = $database_connection -> query("SELECT * FROM GRADES_MODULES WHERE GradeID='$grade_id'");
                    $grade_module_id = $grade_module_id_query -> fetch_assoc();

                    $moduleid = $grade_module_id['ModuleID'];

                    // Getting the module name.
                    $grade_module_query = $database_connection -> query("SELECT * FROM MODULES WHERE ModuleID='$moduleid'");
                    $grade_module = $grade_module_query -> fetch_assoc();

                    // Getting the grade value.
                    $grade_query = $database_connection -> query("SELECT * FROM GRADES WHERE GradeID ='$grade_id'");
                    $grade = $grade_query -> fetch_assoc();

                    echo '<p>Received ' . $grade['GradeValue'] . '% in ' . $grade_module['ModuleName'] . '</p>';
                }


            } else {
                echo "<div class='grades'>";
                echo "<h2>Assign A Grade</h2>";
                echo "</div>";

                // Form used to assign a grade to a student.
                echo "<form class='assignGradeForm' method='POST' action='assigngrade.php'>";
                echo "<input type='textbox' name='StudentID' placeholder='Student ID'/>";
                echo "<input type='textbox' name='ModuleID' placeholder='Module ID'/>";
                echo "<input type='textbox' name='GradeValue' placeholder='Grade Value'/>";
                echo "<input type='submit' name='assign' value='Assign'/>";

                // Output error messages.
                if(isset($_GET['assign'])) {
                    $result = $_GET['assign'];

                    if ($result == 'invalidgrade') {
                        echo ' Invalid Grade.';
                    } elseif ($result == 'invalidmodule') {
                        echo ' Invalid Module ID.';
                    } elseif ($result == 'invalidstudent') {
                        echo ' Invalid Student ID';
                    } elseif ($result == 'empty') {
                        echo ' Missing Details.';
                    } elseif ($result == 'studentmismatch') {
                        echo ' The student you entered is not studying that module.';
                    } elseif ($result == 'unauthorizedmodule') {
                        echo ' You cannot assign a grade on a module that you do not teach.';
                    } elseif ($result == 'success') {
                        echo ' Successfully assigned a grade.';
                    }
                }

                echo "</form>";
            }

            // Seminars Information
            if ($_SESSION['isTeacher']) {
                echo "<div class='seminars'>";
                echo "<h2>Seminars</h2>";
                echo "</div>";
                echo "<b>Attended: </b>";
                
                // Getting the user ID
                $user_id = $user['ID'];

                $query_result = $database_connection -> query("SELECT * FROM SEMINARS INNER JOIN SEMINARS_TEACHERS ON SEMINARS.SeminarID=SEMINARS_TEACHERS.SeminarID WHERE SEMINARS_TEACHERS.TeacherID='$user_id'");

                if($query_result -> num_rows > 0) {
                    while($seminars = $query_result -> fetch_assoc()) {
                        echo '<p>' . $seminars['SeminarName'] . '</p>';
                    }
                } else {
                    echo 'None';
                }

                // Clearing the results from memory.
                $query_result -> free_result();
            }

            // Closing the database connection.
            $database_connection -> close();

            // Closing the session
            session_write_close();
        ?>

    </body>
</html>