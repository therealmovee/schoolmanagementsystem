<?php
    session_start();

    // Load the page only if there was a POST request.
    if (isset($_POST['login'])) {

        // Setting up a Database Connection
        $databaseServer = "localhost";
        $databaseUsername = "root";
        $databasePassword = "";
        $databaseName = "schoolsystem";

        $database_connection = new mysqli($databaseServer, $databaseUsername, $databasePassword, $databaseName);
        
        // Outputting an error incase the connection was not successful.
        if ($database_connection -> connect_error) {
            die("Unsuccessful Connection / Error: " . $database_connection -> connect_error);
        } else {
            echo 'Successful Connection.';

            // Acquiring the username and password from the message body.
            $input_username = $_POST['username'];
            $input_password = $_POST['password'];

            // Checking if any of the given inputs are empty.
            if (empty($input_username) || empty($input_password)) {
                header("Location: ../index.php?login=empty");
                exit();
            }

            // Allowing only alphanumerical characters to be inputted using regular expressions.
            // Also preventing SQL Injection.
            if(!preg_match("/^[a-z-A-Z0-9]*$/", $input_username) || !preg_match("/^[a-z-A-Z0-9]*$/", $input_password)) {
                header("Location: ../index.php?login=invalidcharacters");
                exit();
            }

            // Checking if the given username and password match any database records.
            $query_result = $database_connection -> query("SELECT * FROM PERSON WHERE Username='$input_username' AND Password='$input_password'");

            // If there are more than zero rows than a match exists.
            if ($query_result -> num_rows > 0) {
                // Clearing the last query result.
                $query_result -> free_result();
                
                // Storing session variables.
                $_SESSION['username'] = $input_username;
                $_SESSION['password'] = $input_password;

                // Checking if the is user belongs in the student table or the teacher table
                $query = $database_connection -> query("SELECT * FROM PERSON WHERE Username='$input_username' AND ID IN (SELECT STUDENTS.StudentID FROM STUDENTS)");

                if($query -> num_rows > 0) {
                    $_SESSION['isTeacher'] = false;
                } else {
                    $_SESSION['isTeacher'] = true;
                }

                // Clearing the last query result.
                $query -> free_result();

                header("Location: ../panel.php");
            } else {
                header("Location: ../index.php?login=invalidcredentials");
                exit();
            }
        }

        // Closing the database connection.
        $database_connection -> close();
    }

    session_write_close();
?>